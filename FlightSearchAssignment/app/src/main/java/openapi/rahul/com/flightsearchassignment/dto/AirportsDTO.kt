package openapi.rahul.com.flightsearchassignment.dto

import com.google.gson.annotations.SerializedName

public data class AirportsDTO (
        @SerializedName("DEL") val delhiCode: String,
        @SerializedName("BOM") val mumbaiCode: String
        )