package openapi.rahul.com.flightsearchassignment.views.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_provider.view.*
import openapi.rahul.com.flightsearchassignment.R
import openapi.rahul.com.flightsearchassignment.model.FareModel

class ProviderAdapter(val items: List<FareModel>?, val context: Context) : RecyclerView.Adapter<ProviderViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, pos: Int): ProviderViewHolder {
        return ProviderViewHolder(LayoutInflater.from(context).inflate(R.layout.item_provider
                , parent, false))
    }

    override fun onBindViewHolder(holder: ProviderViewHolder, position: Int) {
        val fareModel = items?.get(position)
        holder.tvFare.text = fareModel?.fare
        holder.tvProvider.text = fareModel?.provider
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items?.size!!
    }

}

class ProviderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val tvProvider = view.tv_provider_name
    val tvFare = view.tv_provider_fare

}