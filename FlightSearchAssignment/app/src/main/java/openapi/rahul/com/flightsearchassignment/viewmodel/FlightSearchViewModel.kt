package openapi.rahul.com.flightsearchassignment.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import openapi.rahul.com.flightsearchassignment.ModelInterface
import openapi.rahul.com.flightsearchassignment.dto.AppendixDTO
import openapi.rahul.com.flightsearchassignment.dto.FlightDTO
import openapi.rahul.com.flightsearchassignment.model.FareModel
import openapi.rahul.com.flightsearchassignment.model.FlightDataModel
import openapi.rahul.com.flightsearchassignment.repository.FlightSearchRepo
import openapi.rahul.com.flightsearchassignment.util.AppUtil

class FlightSearchViewModel : ViewModel(), ModelInterface {

    private var mFlightLiveData: MutableLiveData<ArrayList<FlightDataModel>> = MutableLiveData()
    private var mFlightErrorLiveData: MutableLiveData<String> = MutableLiveData()
    private val mFlightRepo: FlightSearchRepo = FlightSearchRepo(this)

    override fun getData(flightDTO: FlightDTO?) {
        filterDataForView(flightDTO)
    }

    override fun getError(error: String?) {
        mFlightErrorLiveData.value = error
    }

    public fun getFlightData() {
        mFlightRepo.getFlightListData()
    }

    private fun filterDataForView(flightDTO: FlightDTO?) {

        val appendixDTO = flightDTO?.appendixDTO
        val flightList: ArrayList<FlightDataModel> = ArrayList()

        flightDTO?.flightListDTO?.forEach {

            val flightDataModel = FlightDataModel()

            flightDataModel.arrivalTime = AppUtil.getDate(it.arrivalTime, "HH:mm:ss")
            flightDataModel.departureTime = AppUtil.getDate(it.departureTime, "HH:mm:ss")
            flightDataModel.seatClass = it.flightClass
            flightDataModel.flightName = getAirline(it.airlineCode, appendixDTO)
            flightDataModel.originCode = it.originCode
            flightDataModel.destCode = it.destinationCode

            val fareModelList: ArrayList<FareModel> = ArrayList()

            it.faresList.forEach {
                val fareModel = FareModel()
                val provider = getProvider(it.providerId, appendixDTO)
                fareModel.provider = provider
                fareModel.fare = it.fare.toString()
                fareModelList.add(fareModel)
            }

            flightDataModel.fareList = fareModelList
            flightList.add(flightDataModel)
        }

        mFlightLiveData.value = flightList

    }

    private fun getProvider(providerId: Int, appendixDTO: AppendixDTO?): String? {
        when (providerId) {
            1 -> return appendixDTO?.providersDTO?.provideCodeOne
            2 -> return appendixDTO?.providersDTO?.provideCodeTwo
            3 -> return appendixDTO?.providersDTO?.provideCodeThree
            4 -> return appendixDTO?.providersDTO?.provideCodeFour
        }
        return ""
    }

    private fun getAirline(airlineCode: String, appendixDTO: AppendixDTO?): String? {
        when (airlineCode) {
            "SG" -> return appendixDTO?.airlinesDTO?.spiceJetCode
            "AI" -> return appendixDTO?.airlinesDTO?.airIndiaCode
            "G8" -> return appendixDTO?.airlinesDTO?.goAirCode
            "9W" -> return appendixDTO?.airlinesDTO?.jetAirwaysCode
            "6E" -> return appendixDTO?.airlinesDTO?.indigoCode
        }
        return ""
    }

    public fun getFlightLiveData(): LiveData<ArrayList<FlightDataModel>> {
        return mFlightLiveData;
    }

    public fun getFlightErrorLiveData(): LiveData<String> {
        return mFlightErrorLiveData;
    }

}
