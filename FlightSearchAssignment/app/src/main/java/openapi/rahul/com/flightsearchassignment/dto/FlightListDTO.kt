package openapi.rahul.com.flightsearchassignment.dto

import com.google.gson.annotations.SerializedName

public data class FlightListDTO (
        @SerializedName("originCode") val originCode: String,
        @SerializedName("destinationCode") val destinationCode: String,
        @SerializedName("departureTime") val departureTime: Long,
        @SerializedName("arrivalTime") val arrivalTime: Long,
        @SerializedName("fares") val faresList: List<FaresDTO>,
        @SerializedName("airlineCode") val airlineCode: String,
        @SerializedName("class") val flightClass: String
        )