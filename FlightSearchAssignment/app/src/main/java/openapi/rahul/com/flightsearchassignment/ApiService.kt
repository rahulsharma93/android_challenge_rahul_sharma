package openapi.rahul.com.flightsearchassignment

import openapi.rahul.com.flightsearchassignment.dto.FlightDTO
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiService {

    @GET("v2/5979c6731100001e039edcb3")
    fun getFlightList(): Call<FlightDTO>


    companion object Factory {

        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://www.mocky.io/")
                    .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}
