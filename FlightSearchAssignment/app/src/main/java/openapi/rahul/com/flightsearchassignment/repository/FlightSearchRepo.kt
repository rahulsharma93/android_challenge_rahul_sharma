package openapi.rahul.com.flightsearchassignment.repository

import openapi.rahul.com.flightsearchassignment.ApiService
import openapi.rahul.com.flightsearchassignment.ModelInterface
import openapi.rahul.com.flightsearchassignment.dto.FlightDTO
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

public class FlightSearchRepo(private val modelInterface: ModelInterface) {

    private val apiService: ApiService = ApiService.create()

    fun getFlightListData() {
        val call = apiService.getFlightList()

        call.enqueue(object : Callback<FlightDTO> {
            override fun onResponse(call: Call<FlightDTO>, response: Response<FlightDTO>) {
                modelInterface.getData(response.body())
            }

            override fun onFailure(call: Call<FlightDTO>, t: Throwable) {
                modelInterface.getError(t.message)
            }
        })


    }
}