package openapi.rahul.com.flightsearchassignment.model

class FlightDataModel {
    var flightName: String? = null
    var departureTime: String? = null
    var arrivalTime: String? = null
    var seatClass: String? = null
    var fareList: List<FareModel>? = null
    var originCode: String?=null
    var destCode:String? = null
}
