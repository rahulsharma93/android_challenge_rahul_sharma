package openapi.rahul.com.flightsearchassignment.dto

import com.google.gson.annotations.SerializedName

data class FlightDTO(
        @SerializedName("appendix") val appendixDTO: AppendixDTO,
        @SerializedName("flights") val flightListDTO: List<FlightListDTO>
)