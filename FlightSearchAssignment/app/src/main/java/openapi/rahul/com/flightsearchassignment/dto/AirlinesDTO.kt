package openapi.rahul.com.flightsearchassignment.dto

import com.google.gson.annotations.SerializedName

public data class AirlinesDTO(
        @SerializedName("SG") val spiceJetCode: String,
        @SerializedName("AI") val airIndiaCode: String,
        @SerializedName("G8") val goAirCode: String,
        @SerializedName("9W") val jetAirwaysCode: String,
        @SerializedName("6E") val indigoCode: String
        )