package openapi.rahul.com.flightsearchassignment.dto

import com.google.gson.annotations.SerializedName

public class FaresDTO (
        @SerializedName("providerId") val providerId: Int,
        @SerializedName("fare") val fare: Int
        )