package openapi.rahul.com.flightsearchassignment

import openapi.rahul.com.flightsearchassignment.dto.FlightDTO

interface ModelInterface {

    fun getData(flightDTO: FlightDTO?)
    fun getError(error: String?)

}