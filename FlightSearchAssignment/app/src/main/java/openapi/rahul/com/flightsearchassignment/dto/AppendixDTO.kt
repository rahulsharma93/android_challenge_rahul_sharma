package openapi.rahul.com.flightsearchassignment.dto

import com.google.gson.annotations.SerializedName

data class AppendixDTO(
        @SerializedName("airlines") val airlinesDTO: AirlinesDTO,
        @SerializedName("airports") val airportsDTO: AirportsDTO,
        @SerializedName("providers") val providersDTO: ProvidersDTO

)