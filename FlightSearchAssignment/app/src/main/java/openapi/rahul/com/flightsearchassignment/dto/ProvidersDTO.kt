package openapi.rahul.com.flightsearchassignment.dto

import com.google.gson.annotations.SerializedName

public data class ProvidersDTO (
        @SerializedName("1") val provideCodeOne: String,
        @SerializedName("2") val provideCodeTwo: String,
        @SerializedName("3") val provideCodeThree: String,
        @SerializedName("4") val provideCodeFour: String
        )