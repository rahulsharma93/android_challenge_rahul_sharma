package openapi.rahul.com.flightsearchassignment.views.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import openapi.rahul.com.flightsearchassignment.R
import openapi.rahul.com.flightsearchassignment.model.FlightDataModel
import openapi.rahul.com.flightsearchassignment.viewmodel.FlightSearchViewModel
import openapi.rahul.com.flightsearchassignment.views.adapters.FlightRecyclerViewAdapter

class MainActivity : AppCompatActivity() {

    private var mFlightSearchViewModel: FlightSearchViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mFlightSearchViewModel = ViewModelProviders.of(this).get(FlightSearchViewModel::class.java)
        observerFlightData()
    }

    override fun onStart() {
        super.onStart()
        mFlightSearchViewModel?.getFlightData()
    }


    private fun observerFlightData() {
        progress_bar.visibility = View.VISIBLE
        mFlightSearchViewModel?.getFlightLiveData()?.observe(this, Observer {
            progress_bar.visibility = View.GONE
            populateData(it)
        })

        mFlightSearchViewModel?.getFlightErrorLiveData()?.observe(this, Observer {
            progress_bar.visibility = View.GONE
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun populateData(flightDataModelList: ArrayList<FlightDataModel>?) {

        tv_source.text = flightDataModelList?.get(0)?.originCode
        tv_destination.text = flightDataModelList?.get(0)?.destCode

        recycler_view_flights.apply {
            layoutManager = LinearLayoutManager(context)
            // set the custom adapter to the RecyclerView
            adapter = FlightRecyclerViewAdapter(flightDataModelList, context)
        }


    }
}
