package openapi.rahul.com.flightsearchassignment.util

import java.text.SimpleDateFormat
import java.util.*


class AppUtil{

    companion object {
        fun getDate(milliSeconds: Long, dateFormat: String): String {
            // Create a DateFormatter object for displaying date in specified format.
            val formatter = SimpleDateFormat(dateFormat)
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            return formatter.format(calendar.time)
        }
    }
}
