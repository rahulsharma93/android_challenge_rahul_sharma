package openapi.rahul.com.flightsearchassignment.views.adapters

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.flight_item_recycler_view.view.*
import openapi.rahul.com.flightsearchassignment.R
import openapi.rahul.com.flightsearchassignment.model.FlightDataModel


class FlightRecyclerViewAdapter(val items: ArrayList<FlightDataModel>?, val context: Context) : RecyclerView.Adapter<FlightViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, pos: Int): FlightViewHolder {
        return FlightViewHolder(LayoutInflater.from(context).inflate(R.layout.flight_item_recycler_view, parent, false))
    }

    override fun onBindViewHolder(holder: FlightViewHolder, position: Int) {
        val flightDataModel = items?.get(position)
        holder.tvAirLine.text = flightDataModel?.flightName
        holder.tvArrivalTime.text = flightDataModel?.arrivalTime
        holder.tvDepartureTime.text = flightDataModel?.departureTime
        holder.tvSeatClass.text = flightDataModel?.seatClass
        holder.rvFares.apply {
            layoutManager = LinearLayoutManager(context)
            // set the custom adapter to the RecyclerView
            adapter = ProviderAdapter(flightDataModel?.fareList,context)
        }

    }

    override fun getItemCount(): Int {
        return items?.size!!
    }

}

class FlightViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val tvDepartureTime = view.tv_departure_time
    val tvArrivalTime = view.tv_arrival_time
    val tvAirLine = view.tv_airline
    val tvSeatClass = view.tv_class
    val rvFares = view.rv_fares
}
